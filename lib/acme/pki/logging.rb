module Acme
  class PKI
    module Logging
      class Logger
        SEVERITIES   = {
          DEBUG: { level: 0, color: :light_black },
          INFO:  { level: 10, color: nil },
          WARN:  { level: 20, color: :yellow },
          ERROR: { level: 30, color: :red },
          FATAL: { level: 40, color: { color: :white, background: :red } }
        }.freeze
        LEVELS       = SEVERITIES.transform_values { |v| v.fetch :level }.freeze
        COLORS       = SEVERITIES.collect do |severity, config|
          color = config.fetch :color
          color = severity.to_s.colorize color
          [severity, color]
        end.to_h.freeze
        STDERR_LEVEL = LEVELS.fetch(:ERROR).freeze

        def initialize(severity)
          severity = severity.to_s.upcase.to_sym
          @level   = LEVELS.fetch severity
        end

        def io(severity)
          level = LEVELS.fetch severity
          level >= STDERR_LEVEL ? $stderr : $stdout
        end

        def format(severity, message)
          level = LEVELS.fetch severity
          return if @level > level
          color = COLORS.fetch severity
          "#{color}: #{message}"
        end

        def log(severity, message)
          io      = self.io severity
          message = self.format severity, message
          io.puts message
        end

        SEVERITIES.keys.each do |severity|
          define_method severity.to_s.downcase do |message|
            self.log severity, "#{message}"
          end
        end

        def process(message, severity = :INFO)
          message = self.format severity, "#{message}... "
          io      = self.io severity
          io.print message
          io.flush
          result = yield
          io.puts "[#{'OK'.colorize :green}]\n"
          result
        rescue => e
          io.puts "[#{'KO'.colorize :red}]\n"
          self.log :ERROR, e
          raise e
        end
      end

      def logger
        Logging.logger
      end

      def self.logger
        @logger ||= Logger.new ENV.fetch 'ACME_LOGGING_LEVEL', 'info'
      end
    end
  end
end
