require 'resolv'
require 'acme/pki/logging'

module Acme
  class PKI
    class DnsUpdate
      include Logging

      def initialize(server, key)
        @server = server
        @key    = key
      end

      def update(domain, *cmd)
        knsupdate = ['knsupdate', '-k', @key]
        cmd       = [
          "server #{@server}",
          "zone #{domain}",
          "origin #{domain}",
          *cmd,
          "send",
          "answer"
        ].join "\n"

        logger.debug knsupdate
        logger.debug cmd

        o, e, s = Open3.capture3 *knsupdate, stdin_data: cmd
        logger.debug o
        unless s.success?
          logger.error e
          raise Exception, e
        end
      end

      WAIT_COUNT = 60
      WAIT_DELAY = 1

      def wait_ns(ns, domain, name, type, content)
        logger.info "Wait #{type.to_s.colorize :yellow} #{name.colorize :yellow} on NS #{ns.colorize :yellow}"
        ns_resolver = Resolv::DNS.new nameserver: ns, search: domain

        WAIT_COUNT.times do |n|
          resources = ns_resolver.getresources(name, type).collect &:data
          logger.debug "#{n}/#{WAIT_COUNT}: #{resources}"
          return if resources.include? content
          sleep WAIT_DELAY
        end

        raise Exception, "Unable to find #{content.colorize :yellow} on #{type.colorize :yellow} #{name.colorize :yellow} using NS #{ns.colorize :yellow}, getting #{resources}" if n >= count
      end

      def wait(domain, name, type, content)
        name = "#{name}.#{domain}"
        type = Resolv::DNS::Resource::IN.const_get type
        logger.info "Wait for #{type.to_s.colorize :yellow} #{name.colorize :yellow} #{content.colorize :yellow} DNS propagation"
        system_resolver = Resolv::DNS.new
        system_resolver.each_resource domain, Resolv::DNS::Resource::IN::NS do |resource|
          ns = resource.name.to_s
          self.wait_ns ns, domain, name, type, content
        end
      end

      def add(domain, name, type, content, ttl = '1m')
        logger.info "Add #{name.colorize :yellow} #{ttl.colorize :yellow} #{type.colorize :yellow} #{content.colorize :yellow} into #{domain.colorize :yellow} DNS zone"
        self.update domain, "add #{name} #{ttl} #{type} #{content}"
        self.wait domain, name, type, content
      end

      def delete(domain, name, type, content)
        logger.info "Delete #{name.colorize :yellow} #{type.colorize :yellow} #{content.colorize :yellow} into #{domain.colorize :yellow} DNS zone"
        self.update domain, "delete #{name} #{type} #{content}"
      end

      def add_then_remove(domain, name, type, content, ttl = '1m')
        self.add domain, name, type, content, ttl
        yield
      ensure
        self.delete domain, name, type, content
      end
    end
  end
end
